package com.simplon.restapi.dao;

import com.simplon.restapi.model.Rabbit;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RabbitRepository extends JpaRepository<Rabbit, Integer> {
    // Cherche un lapin par mot-clef (kw) sur les différentes pages
    @Query("SELECT r FROM Rabbit r WHERE r.name LIKE :x")
    public Page<Rabbit> searchRabbitByKeyWord(@Param("x") String kw, Pageable pageable);
}
