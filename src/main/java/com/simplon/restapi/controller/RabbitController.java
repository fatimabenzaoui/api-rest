package com.simplon.restapi.controller;

import java.util.List;

import com.simplon.restapi.dao.RabbitRepository;
import com.simplon.restapi.model.Rabbit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
// corrige l'erreur "Access to XMLHttpRequest blocked by CORS policy" (faille XSS)
@CrossOrigin("*")
@RequestMapping("/api")
public class RabbitController {

    // Injecte le RabbitRepository
    @Autowired
    private RabbitRepository rabbitRepository;

    // Affiche la liste des lapins
    @GetMapping(value="/rabbits")
    public List<Rabbit> getAllRabbits() {
        return rabbitRepository.findAll();
    }

    // Affiche un lapin avec son id
    @GetMapping(value="/rabbits/{id}")
    public Rabbit getRabbitById(@PathVariable Integer id) {
        return rabbitRepository.findById(id).orElse(null);
    }

    // Ajoute un lapin
    @PostMapping(value="/rabbits")
    public Rabbit addRabbit(@RequestBody Rabbit r) {
        return rabbitRepository.save(r);
    }

    // Modifie un lapin
    @PutMapping(value = "/rabbits/{id}")
    public Rabbit save(@PathVariable Integer id, @RequestBody Rabbit r) {
        r.setId(id);
        return rabbitRepository.save(r);
    }

    // Supprime un lapin
    @DeleteMapping(value = "/rabbits/{id}")
    public void deleteRabbit(@PathVariable Integer id) {
        rabbitRepository.deleteById(id);
    }

    // Cherche un lapin par mot-clef
    @GetMapping(value="/rabbits/search")
    public Page<Rabbit> searchRabbitByKeyWord(
        @RequestParam(name = "kw", defaultValue = "") String kw, 
        @RequestParam(name = "page", defaultValue = "0") int page, 
        @RequestParam(name = "size", defaultValue = "5") int size
    ) {
        return rabbitRepository.searchRabbitByKeyWord("%"+kw+"%", PageRequest.of(page, size));
    }

}