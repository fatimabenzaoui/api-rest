package com.simplon.restapi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.simplon.restapi.dao.RabbitRepository;
import com.simplon.restapi.model.Rabbit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestapiApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(RestapiApplication.class, args);
	}

	@Autowired
	private RabbitRepository rabbitRepository;

	@Override
	public void run(String... args) throws Exception {
		// Pour insérer une date
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		// Insérer des données
		rabbitRepository.save(new Rabbit(null, "Jojo", dateFormat.parse("12/01/2015")));
		rabbitRepository.save(new Rabbit(null, "Pantoufle", dateFormat.parse("17/03/2018")));
		rabbitRepository.save(new Rabbit(null, "Perle", dateFormat.parse("15/07/2020")));
		// Afficher les données
		rabbitRepository.findAll().forEach(r -> 
			System.out.println(r.getName())
		);
	}

}
