OBJECTIF : Créer une API REST avec Spring Boot

Installer les dépendances ds le projet Spring Boot : 
    - Spring Web 
    - Spring Boot DevTools 
    - Spring Data JPA 
    - H2 Database

Installer l'extension Chrome JSON Formatter 

Créer une entité JPA

Tester son entité JPA

Créer une interface DAO implémentant JPARepository

Tester son interface DAO directement ds l'appli 
grâce à l'interface CommandLineRunner

Créer un Rest Controller

Implémenter la méthode searchRabbitByKeyWord() dans l'interface DAO

Tester son API REST avec REST Client (Extension VS Code)
